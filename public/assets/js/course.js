console.log('Hello from JS!')

// the first thing that we need to do is to identify which course it needs to display inside the browser.
// we are going to use the courseId to identify the correct course properly. 
let params = new URLSearchParams(window.location.search)
// window.location - returns a location object with information about the "current" location of the document.
// .search property - contains the query string section of the current URL. 
// search property return an object of type stringString.
// URLSearchParams() - method/constructor that creates and returns a new URLSearchParams object. (this is a class)
// "URLSearchParams" - describes the interface that defines utility methods to work with the query string of a URL. (this is a prop type)
// new - instantiate a user-defined object.
// yes it is a class - template for creating the object.

//  params = {
//  	"courseId": ...id of course that we passed
// }
let courseId = params.get('courseId')
console.log(courseId)

// let's capture the sections of the html body
let name = document.querySelector('#courseName')
let desc = document.querySelector('#courseDesc')
let price = document.querySelector('#coursePrice')
let enroll = document.querySelector("#enrollmentContainer")
// let's capture the access token from the local storage
let token = localStorage.getItem('token')
console.log(token)
let cardFooter;


fetch(`https://ancient-sands-24184.herokuapp.com/api/courses/${courseId}`).then(res => res.json()).then(data => {
	console.log(data)

	name.innerHTML = data.name
	desc.innerHTML = data.description
	price.innerHTML = data.price
	// enroll.innerHTML = `
 //      <form id="Enroll">
 //        <button type="submit" class="btn btn-block btn-primary my-3">Enroll</button>
 //      </form> `;
     enroll.innerHTML = ` 
      <a id="Enroll" class="btn btn-info text-white btn-block">Enroll</a>
      `

      // we have to capture first the button for enroll, add an event listener to trigger an event. create a function in the eventlistener() to describe the next set of procedures.

let enrollBtn = document.querySelector("#Enroll");
	// enrollBtn.addEventListener("submit", (e) => {
	enrollBtn.addEventListener("click", (e) => {
        e.preventDefault();
fetch(`https://ancient-sands-24184.herokuapp.com/api/users/enroll`, {
          method: 'POST',
          headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${token}`
          },
          body: JSON.stringify({
            courseId
          })
        }).then(res => {
          return res.json();
        }).then(data => {
          console.log(data);
          if(data === true) {
          	Swal.fire({
          		icon: "success",
          		title: "Success!",
          		text: "You are now enrolled!",
          		confirmButtonText: "Back to courses",
          	}).then(() => {
          		window.location.href="./courses.html";
          	});
          }else {
          	// inform the user that the req has failed
          	Swal.fire({
          		icon: "error",
          		title: "Error occured!",
          		text: "Something went wrong!"
          	})
          }
        });
      });

})

// if the result is displayed in the console. it means you were able to properly pass the courseId and successfully created your fetch request.

// what you're going to do here is to insert the enroll button component inside this page.



