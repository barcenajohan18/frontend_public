// console.log("linked to the JS module");

//ADVANCE TASK ANSWER KEY:

let formClear = () => {
	document.querySelector("#courseName").value = "";
	document.querySelector("#coursePrice").value = "";
	document.querySelector("#courseDescription").value = "";
}

let formSubmit = document.querySelector("#createCourse");

formSubmit.addEventListener("submit", (event) => {
	
	event.preventDefault()//which is used to avoid automatic page redirection

	let name = document.querySelector("#courseName").value;
	// console.log(name)
	let price = document.querySelector("#coursePrice").value;
	// console.log(course)
	let description = document.querySelector("#courseDescription").value;
	// console.log(description)

	//save the new entry inside the database. By describing the request method/structure

	fetch('https://ancient-sands-24184.herokuapp.com/api/courses/course-exists', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			name: name.trim()		
		})
	}).then(response => response.json()
            ) //this will give the information if there are no duplicates found.
	.then(data => {
		if(data === false){
			fetch('https://ancient-sands-24184.herokuapp.com/api/courses/addCourse', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					name: name,
					description: description,
					price: price
				})
			}).then(response => {return response.json()
		 //after describing the structure of the request body, now create the structure of the response of the response coming from the back end.
		}).then(data => {
			console.log(data);
			if(data === true) {
				Swal.fire({
					icon: 'success',
					title: 'Success',
					text: 'Course successfully added!'
				});
				formClear();
			} else {
				Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong...'
        });
			}
		})
	} else {
		Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Course Already Exists!'
        });
	}
})
})	
