console.log("Hello from courses.js");
let modalButton = document.querySelector('#adminButton')
//CAPTURE the html body which will display the content coming from the database.

let container = document.querySelector('#coursesContainer')
//we are going to take the value of the of the isAdmin property from the local storage
let cardFooter;

let isAdmin = localStorage.getItem("isAdmin")

if(isAdmin == "false" || !isAdmin){
	//if a user is a regular user, do not show the addcourse button.
	modalButton.innerHTML = null;
}else{
	modalButton.innerHTML = `
		<div class="col-md-2 offset-md-10 text-white"><a href="./addCourse.html" class="btn btn-block btn-danger">Add Course</a></div>
	`
}

fetch('https://ancient-sands-24184.herokuapp.com/api/courses/').then(res => res.json()).then(data => {
	console.log(data);
	//declare a variable that will display a result in the browswer depending on the return
	let courseData;
	//create a control structure that wi;; determine the value that the variable will hold.
	if(data.length < 1){
		courseData = "No Course Available"
	}else{
		//we will iterate the courses collection and display each course inside the browser
		courseData = data.map(course => {
			//lets check the make up of each element inside the courses collection
			console.log(course._id);

			//if the user is a regular user, display the enroll button and display course button.
			if(isAdmin == "false" || !isAdmin)
			{
				cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-dark text-white btn-block">
                            View Course Details</a>
                            `
			}else{
				cardFooter = `
				<a href="./editCourse.html?courseId=${course._id}" class="btn btn-dark text-white btn-block">
                            Edit</a>
                <a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block">
                            Disable Course </a>
				`
			}
			return (
				`
            <div class="col-md-6 my-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"> 
                        ${course.name}
                        </h5>
              
                        <p class="card-text text-left">
                        ₱ ${course.price}
                        </p>
                        
                    </div>
                    <div class="card-footer">
                        ${cardFooter}
                    </div>
                </div>
            </div>
				` //we attached a query string in the URL which allows us to embed the ID from the database record into the query string.
				// ? -> inside the URL "acts" as a "separator", it indicates the end of a URL resource path, and indicates the start of the *query string*.
				// # -> this was originally "used" to jump to a specific elemnent with same id name/value.
				)
		}).join("")// we used the join() to create a return of a new string
		//it contatenated all the objects inside the array and converted each to a string data type.
	}
	container.innerHTML = courseData;
})