console.log("Hello from deleteCourse.js")

let params = new URLSearchParams(window.location.search);

let idCheck = params.has('courseId');
console.log(params.has('courseId'));//returns true if exists

let courseId = params.get('courseId');
console.log(...courseId);

let token = localStorage.getItem('token');

fetch(`https://ancient-sands-24184.herokuapp.com/api/courses/${courseId}`, {
	method: 'DELETE',
	headers: {
		'Authorization': `Bearer ${token}`
	}
}).then(res => {
	return res.json()
}).then(data => {
    //creation of new course successful
    if(data === true){
        //redirect to courses index page
        window.location.replace('./courses.html')
    }
    else{
        //error in creating course, redirect to error page
        alert('something went wrong')
    }
})
