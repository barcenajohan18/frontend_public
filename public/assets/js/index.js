console.log("Hi from Index.js")

let navItem = document.querySelector('#navSession')

//lets take the access token from the local storage property.
let userToken = localStorage.getItem("token")
console.log(userToken)

//lets create a control structure that will determine which elements inside the nav bar will be displayed if a user token is found in the local storage.
if(!userToken) {
	navItem.innerHTML = 
	`
	<li class ="nav-item">
		<a href="./pages/login.html" class="nav-link">Log in</a>
	</li>
	`
}else{
	navItem.innerHTML =
	`
	<li class ="nav-item">
		<a href="./pages/logout.html" class="nav-link">Log out</a>
	</li>
	`
}