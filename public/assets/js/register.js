console.log("Hello from JS file");

let registerUserForm = document.querySelector('#registerUser');

registerUserForm.addEventListener("submit", (e) => {
    
    e.preventDefault();

    let fName = document.querySelector('#firstName').value
    let lName = document.querySelector('#lastName').value
    let uEmail = document.querySelector('#userEmail').value
    let mNumber = document.querySelector('#mobileNumber').value
    let pw1 = document.querySelector('#password1').value
    let pw2 = document.querySelector('#password2').value

    if ((pw1 !== "" && pw2 !== "") && (pw1 === pw2) && (mNumber.length === 11)) {
        fetch('https://ancient-sands-24184.herokuapp.com/api/users/email-exists', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: uEmail
            })
        }).then(res => res.json()
            ) //this will give the information if there are no duplicates found.
            .then(data => {
                if(data === false){
                    fetch("https://ancient-sands-24184.herokuapp.com/api/users/register", {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            firstName: fName,
                            lastName: lName,
                            email: uEmail,
                            mobileNo: mNumber,
                            password: pw1
                        })
                    }).then(res => {
                        return res.json()
                    }).then(data => {
                        
                        if(data === true) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Success',
                                text: 'Email sucessfully registered'
                            });
                            window.location.replace('./login.html')
                        } else {
                            Swal.fire("Something went wrong!")
                        }
                    })
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Email currently exist, please use a different email'
                    });
                }
            })
    } else if((pw1 !== "" && pw2 !== "") && (pw1 !== pw2)) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Passwords do not match!'
        });
    } else if((mNumber.length !== 11)) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Incorrect Number Format!'
        });
    } else {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Please enter details first!'
        });
    }
})

/*if((password !== "" && verifyPassword !== "")&&(verifyPassword === password}){
    alert
} else {

})*/
